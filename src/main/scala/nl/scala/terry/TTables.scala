package nl.scala.terry
import slick.driver.H2Driver.api._
import slick.lifted.ProvenShape

object TTables {
  val paymentAccounts = TableQuery[PaymentAccounts]
  val payments = TableQuery[Payments]
}

case class Payment(id:String, accountId:String, currency:String, amount:Double)
class Payments(tag:Tag) extends Table[Payment](tag, "payments")
{
  def id = column[String]("id")
  def accountId = column[String]("account_id")
  def currency = column[String]("currency")
  def amount = column[Double]("amount")

  def * = (id, accountId, currency, amount) <> (Payment.tupled, Payment.unapply)

  def accountFK = foreignKey("account_fk", accountId, TTables.paymentAccounts)(_.id)
}

case class PaymentAccount(id:String, name:String)
class PaymentAccounts(tag:Tag) extends Table[PaymentAccount](tag, "payment_accounts")
{
  def id = column[String]("id")
  def name = column[String]("name")

  override def * : ProvenShape[PaymentAccount] = (id,name) <> (PaymentAccount.tupled, PaymentAccount.unapply)
}
