package nl.scala.terry
import org.scalatest._
import org.scalatest.time.{Span, Seconds}
import org.scalatest.concurrent.{Eventually, ScalaFutures}
import scala.concurrent.duration._
import slick.driver.H2Driver.api._
import scala.concurrent.ExecutionContext.Implicits.global
import slick.jdbc.meta.MTable
import scala.concurrent.{Await, Future}
import java.util.UUID
import java.sql.SQLException

/**
 * Created by terryhendrix on 24/08/15.
 */
class TTablesSuite extends FeatureSpec with ScalaFutures with Eventually with BeforeAndAfterEach with Matchers with GivenWhenThen
{
  implicit override val patienceConfig = PatienceConfig(timeout = Span(5, Seconds))
  var db:Database = _

  feature("Try out slick") {
    scenario("1. Create the database model") {
      When("Creating the payments accounts schema")

      Then("Slick creates DDL")
      TTables.paymentAccounts.schema.createStatements.foreach(println)

      When("Creating the payments accounts schema")
      Then("Slick creates DDL")
      TTables.payments.schema.createStatements.foreach(println)
    }

    scenario("2. Insert some data into payment_accounts & payments") {
      When("A payment_account is created")
      val account = PaymentAccount(UUID.randomUUID.toString, "Terry Hendrix")
      val insertAccount = TTables.paymentAccounts += account
      insertAccount.statements.map(println)

      val payment = Payment(UUID.randomUUID.toString, account.id, "EUR", 19.95)
      val insertPayment = TTables.payments += payment
      insertPayment.statements.map(println)

      db.run(DBIO.seq(
        insertAccount,
        insertPayment
      )).futureValue

      eventually {
        db.run(TTables.paymentAccounts.result).futureValue.apply(0) shouldBe account
        db.run(TTables.payments.result).futureValue.apply(0) shouldBe payment
      }
    }

    scenario("3. Throw an SqlException when foreign keys constraint is violated") {
      When("A payment_account is created")
      val payment = Payment(UUID.randomUUID.toString, UUID.randomUUID().toString, "EUR", 19.95)
      val insertPayment = TTables.payments += payment
      insertPayment.statements.map(println)

      intercept[SQLException] {
        Await.result(db.run(DBIO.seq(
          insertPayment
        )), 5 seconds)
      }
    }

    scenario("4. Execute some queries") {
      When("Payments are inserted for accounts terry and henk")
      val terry = PaymentAccount(UUID.randomUUID.toString, "Terry Hendrix")
      val terryPayment1 = Payment(UUID.randomUUID.toString, terry.id, "EUR", 19.95)
      val terryPayment2 = Payment(UUID.randomUUID.toString, terry.id, "EUR", 10.05)
      val terryPayment3 = Payment(UUID.randomUUID.toString, terry.id, "EUR", 10)
      val henk = PaymentAccount(UUID.randomUUID.toString, "Henk de Vries")
      val henkPayment1 = Payment(UUID.randomUUID.toString, henk.id, "EUR", 19.95)
      val henkPayment2 = Payment(UUID.randomUUID.toString, henk.id, "US", 20.05)

      val insertAccounts = TTables.paymentAccounts ++= Seq(
        terry,
        henk
      )
      insertAccounts.statements.map(println)

      val insertPayment = TTables.payments ++= Seq(
        terryPayment1,
        terryPayment2,
        terryPayment3,
        henkPayment1,
        henkPayment2
      )
      insertPayment.statements.map(println)

      db.run(DBIO.seq(
        insertAccounts,
        insertPayment
      )).futureValue

      Then("There should be 5 payments in total")
      val totalPayments = TTables.payments.map(_.id).countDistinct.result
      totalPayments.statements.foreach(println)
      db.run(totalPayments).futureValue shouldBe 5

      Then("We should be able to find 3 payments for terry")
      val terryPayments = TTables.payments.filter(_.accountId === terry.id).result
      terryPayments.statements.foreach(println)
      val resTerryPayments = db.run(terryPayments).futureValue
      println(resTerryPayments)
      resTerryPayments.size shouldBe 3
      resTerryPayments should contain(terryPayment1)
      resTerryPayments should contain(terryPayment2)
      resTerryPayments should contain(terryPayment3)

      Then("We should be able to find 2 payments for henk")
      val henkPayments = TTables.payments.filter(_.accountId === henk.id).result
      henkPayments.statements.foreach(println)
      val henkPaymentsRes = db.run(henkPayments).futureValue
      println(henkPaymentsRes)
      henkPaymentsRes.size shouldBe 2
      henkPaymentsRes should contain(henkPayment1)
      henkPaymentsRes should contain(henkPayment2)

      Then("terry should have payed a total of 40 EUR")
      val terryTotalPayed = TTables.payments.filter(_.accountId === terry.id)
        .groupBy(_.currency)
        .map(t => (t._1, t._2.map(_.amount).sum))
        .result
      terryTotalPayed.statements.foreach(println)
      val terryTotalPayedRes = db.run(terryTotalPayed).futureValue
      println(terryTotalPayedRes)
      terryTotalPayedRes should contain(("EUR", Some(40.0)))

      Then("henk should have payed a total of 19.95 EUR and 20.05 US")
      val henkTotalPayed = TTables.payments.filter(_.accountId === henk.id)
        .groupBy(_.currency)
        .map(t => (t._1, t._2.map(_.amount).sum))
        .result
      henkTotalPayed.statements.foreach(println)
      val henkTotalPayedRes = db.run(henkTotalPayed).futureValue
      println(henkTotalPayedRes)
      henkTotalPayedRes should contain(("EUR", Some(19.95)))
      henkTotalPayedRes should contain(("US", Some(20.05)))
    }
  }

  def createSchema = {
    Future.sequence(Seq(
      db.run(TTables.payments.schema.create),
      db.run(TTables.paymentAccounts.schema.create)
    )).futureValue

    val res = db.run(MTable.getTables).futureValue
    res.size shouldBe 2
    res(0).name.name shouldBe "payment_accounts"
    res(1).name.name shouldBe "payments"

    val getAccounts = TTables.paymentAccounts.result
    getAccounts.statements.map(println)
    db.run(getAccounts).futureValue.size shouldBe 0

    val getPayments = TTables.payments.result
    getPayments.statements.map(println)
    db.run(getPayments).futureValue.size shouldBe 0
  }

  override def afterEach() = db.close()

  override def beforeEach() = {
    db = Database.forConfig("terry.h2")
    createSchema
  }
}
